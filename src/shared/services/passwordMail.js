import { apiUrls } from "../../config";

export function sendMailPassword(email){

    return fetch(apiUrls.mailForgotPassword, {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(email)
    })
}

